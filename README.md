# zophie

Still in development. This is just a simple CLI Gopher client I'm writing to learn Zig. If you really want to try it,
install Zig 0.11, clone this repo, and use `zig build run` in the project root.
Commands are `q` to quit, `o` to open an address, and `r` to redisplay the last page viewed.
