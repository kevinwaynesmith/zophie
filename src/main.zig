const std = @import("std");
const stdout = std.io.getStdOut().writer();
const stdin = std.io.getStdIn().reader();

const Command = union(enum) {
    access_link: u16,
    quit: void,
    redisplay: void,
    open: void,
};

const GopherLink = struct {
    host: []const u8,
    port: u16,
    path: []const u8,
    link_type: LinkType,
};

const LinkType = enum { dir, txt, search, };

const PageContent = struct {
    text: []const u8,
    links: []const GopherLink,
};

pub fn main() !void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    const allocator = gpa.allocator();
    var current_page: PageContent = undefined;
    while (true) {
        var cmd = getCommand(allocator) catch std.os.exit(1);
        switch(cmd) {
            .quit => {
                try stdout.print("{s}\n", .{randomGoodbye()});
                std.os.exit(0);
            },
            .open => {
                const l: GopherLink = getUserLink(allocator) catch {
                    try stdout.print("Invalid link.\n", .{});
                    continue;
                };
                current_page = try browse(allocator, l);
                try stdout.writeAll(current_page.text);
            },

            .access_link => |idx| {
                if (idx < current_page.links.len) {
                    const link = current_page.links[idx];
                    switch(link.link_type) {
                        .dir => {
                            var old_page = current_page;
                            defer allocator.free(old_page.text);
                            defer allocator.free(old_page.links);
                            current_page = try browse(allocator, link);
                            try stdout.writeAll(current_page.text);
                        },
                        .txt => {
                            const text = try fetchPageRaw(allocator, link);
                            try stdout.writeAll(text);
                        },
                        .search => {
                            var old_page = current_page;
                            defer allocator.free(old_page.text);
                            defer allocator.free(old_page.links);
                            const search_str = try getSearchString(allocator);
                            current_page = try gopherSearch(allocator, link, search_str);
                            try stdout.writeAll(current_page.text);
                        },
                    }
                } else {
                try stdout.print("Invalid link (out of range)\n", .{});
            }
        },
            .redisplay => {
                try stdout.writeAll(current_page.text);
            }
        }
    }
}

fn strToLink(allocator: std.mem.Allocator, str: []const u8) !GopherLink {
    // no regex engine in stdlib as of v0.11 and we don't want to take on a
    // dependency just for this, so we do this the long way

    const start_idx: usize = if (std.mem.eql(u8, str[0..9], "gopher://"))
        9
        else 0;

    const first_slash_idx: ?usize =
        for (str[start_idx..], 0..) |c, i| {
            if (c == '/') break i;
    } else null;

    const colon_idx: ?usize =
        for (str[start_idx..], 0..) |c, i| {
            if (c == ':') break i;
    } else null;

    const host: []const u8 =
        if (first_slash_idx) |fsi| str[start_idx..fsi]
        else if (colon_idx) |ci| str[start_idx..ci]
        else str[start_idx..];

    const path: []const u8 =
        if (first_slash_idx) |fsi| if (colon_idx) |ci|
        str[fsi..ci]
        else str[fsi..]
        else "";

    const port: u16 =
        if (colon_idx) |ci| try std.fmt.parseUnsigned(u16, str[ci + 1..], 10)
        else 70;

    var h: std.ArrayList(u8) = std.ArrayList(u8).init(allocator);
    try h.appendSlice(host);
    var p: std.ArrayList(u8) = std.ArrayList(u8).init(allocator);
    try p.appendSlice(path);

    return GopherLink{
        .host = try h.toOwnedSlice(),
        .path = try p.toOwnedSlice(),
        .port = port,
        .link_type = LinkType.dir,
    };
}

fn gopherSearch(allocator: std.mem.Allocator, link: GopherLink, search_str: []const u8) !PageContent {

    var stream = try std.net.tcpConnectToHost(allocator, link.host, link.port);
    defer stream.close();

    // selector string, tab, search string
    var buffered_writer = std.io.bufferedWriter(stream.writer());
    _ = try buffered_writer.write(link.path);
    _ = try buffered_writer.write("\t");
    _ = try buffered_writer.write(search_str);
    _ = try buffered_writer.write("\r\n"); // gopher request suffix
    try buffered_writer.flush();

    var content = std.ArrayList(u8).init(allocator);
    try stream.reader().readAllArrayList(&content, 1_048_576);

    return try processPage(allocator, try content.toOwnedSlice());
}

fn randomGoodbye() []const u8 {
    const options = [_][]const u8{
        "Shmor al atzmecha!",
        "Goodbye!",
        "À tout à l'heure!",
        "¡Adiós!",
    };

    const utime = @as(u128, @bitCast(std.time.nanoTimestamp()));

    const seed = @as(u64, @truncate(utime));

    const rgen = std.rand.DefaultPrng;
    var rnd =  rgen.init(seed);
    const i = rnd.random().uintLessThan(u8, options.len);

    return options[i];
}

fn browse(allocator: std.mem.Allocator, link: GopherLink) !PageContent {
    const raw_page = try fetchPageRaw(allocator, link);
    const page = try processPage(allocator, raw_page);
    return page;
}

fn fetchPageRaw(allocator: std.mem.Allocator, link: GopherLink) ![]const u8 {
    var stream = try std.net.tcpConnectToHost(allocator, link.host, link.port);
    defer stream.close();

    var buffered_writer = std.io.bufferedWriter(stream.writer());
    _ = try buffered_writer.write(link.path);
    _ = try buffered_writer.write("\r\n"); // gopher request suffix
    try buffered_writer.flush();

    var content = std.ArrayList(u8).init(allocator);
    try stream.reader().readAllArrayList(&content, 1_048_576);

    return content.toOwnedSlice();
}

fn processPage(allocator: std.mem.Allocator, raw_page: []const u8) !PageContent {
    var text = std.ArrayList(u8).init(allocator);
    var links = std.ArrayList(GopherLink).init(allocator);
    var lines = std.mem.splitAny(u8, raw_page, "\n");

    var maybe_line = lines.next();
    while (maybe_line != null and maybe_line.?.len > 0) : (maybe_line = lines.next()) {
        var foo_line = maybe_line.?;
        var line = std.mem.trimRight(u8, foo_line, "\r");
        // gopher delivers tab-separated values
        var tabs: std.mem.SplitIterator(u8, .any) = std.mem.splitAny(u8, line[1..], "\t");
        switch(line[0]) {
            'i' => {
                const display = tabs.next();
                try text.appendSlice(try std.fmt.allocPrint(allocator,
                                                            "        {?s}\n",
                                                            .{display}));
            },
            '0' => {
                const display = tabs.next();

                const link = try processLink(&tabs, LinkType.txt);
                if (link) |l| {

                    try text.appendSlice(try std.fmt.allocPrint(
                        allocator,
                        "TXT-{d: <3} {?s}\n",
                        .{links.items.len, display}));
                    try links.append(l);
                } else {
                    try text.appendSlice(try std.fmt.allocPrint(allocator,
                                                                "TXT???? {?s}\n",
                                                                .{display}));
                }
            },
            '1' => {
                const display = tabs.next();
                const link = try processLink(&tabs, LinkType.dir);
                if (link) |l| {
                    try text.appendSlice(try std.fmt.allocPrint(
                        allocator,
                        "DIR-{d: <3} {?s}\n",
                        .{links.items.len, display}));

                    try links.append(l);
                } else {
                    try text.appendSlice(try std.fmt.allocPrint(allocator,
                                                                "DIR???? {?s}\n",
                                                                .{display}));
                }
            },
            '3' => {
                const display = tabs.next();
                try text.appendSlice(try std.fmt.allocPrint(
                    allocator,
                    "ERR     {?s}\n",
                    .{display}));
            },
            '7' => {
                const display = tabs.next();
                const link = try processLink(&tabs, LinkType.search);

                if (link) |l| {
                    try text.appendSlice(try std.fmt.allocPrint(
                        allocator,
                        "SEA-{d: <3} {?s}\n", .{links.items.len, display}));
                    try links.append(l);
                } else {
                    try text.appendSlice(try std.fmt.allocPrint(allocator,
                                                                "SEA???? {?s}\n",
                                                                .{display}));
                }
            },
            '.' => {
                // signifies end of request, so do nothing
                // not every server sends this
            },
            else => {
                try text.appendSlice(try std.fmt.allocPrint(allocator,
                                                            "{s}\n",
                                                            .{line}));
            }
        }
    }
    return PageContent{.text = try text.toOwnedSlice(),
                       .links = try links.toOwnedSlice(),};
}

fn processLink(tabs: *std.mem.SplitIterator(u8, .any), link_type: LinkType) !?GopherLink {
    const path = tabs.next() orelse return null;
    const host = tabs.next() orelse return null;
    const port_str = tabs.next() orelse return null;
    const port: u16 = std.fmt.parseUnsigned(u16, port_str, 10) catch return null;

    return GopherLink{.host = host,
                      .path = path,
                      .port = port,
                      .link_type = link_type};
}

fn getCommand(allocator: std.mem.Allocator) !Command {
    try stdout.print("zophie>", .{});
    const input = try stdin.readUntilDelimiterAlloc(allocator, '\n', 8);
    const cmd: []const u8 = std.mem.trimRight(u8, input, "\n");

    if (std.mem.eql(u8, cmd, "q") or std.mem.eql(u8, cmd, "Q")) {
        return Command.quit;
    } else if (std.mem.eql(u8, cmd, "r")) {
        return Command.redisplay;
    } else if (std.mem.eql(u8, cmd, "o")) {
        return Command.open;
    } else {
        var link_number: u16 = std.fmt.parseInt(u16, cmd, 10) catch {
            try stdout.print("Invalid command.\n", .{});
            std.os.exit(1);
        };
        return Command{.access_link = link_number};
    }
}

fn getUserLink(allocator: std.mem.Allocator) !GopherLink {
    try stdout.print("Enter address:\n", .{});
    try stdout.print("zophie>", .{});

    var input = std.ArrayList(u8).init(allocator);
    defer input.deinit();

    var writer = input.writer();

    try stdin.streamUntilDelimiter(writer, '\n', 2048);

    return try strToLink(allocator, try input.toOwnedSlice());
}

fn getSearchString(allocator: std.mem.Allocator) ![]const u8 {
    try stdout.print("Enter search string:\n", .{});
    try stdout.print("zophie>", .{});

    var input = std.ArrayList(u8).init(allocator);
    var writer = input.writer();

    try stdin.streamUntilDelimiter(writer, '\n', 2048);

    return input.toOwnedSlice();
}
